package fr.iut;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by gtako on 03/02/17.
 */
public class StringUtil {

    /**
     * Display the number in parameter with the currency format of the "Local.country" chosen.
     * @see String
     * @see NumberFormat
     * @param amount
     * @param locale
     * @return
     */
    public static String prettyCurrencyPrint(final double amount, final Locale locale) {
        String Currency  = String.valueOf(NumberFormat.getCurrencyInstance(locale));
        return Currency;
    }


}
